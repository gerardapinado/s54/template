let collection = [];

// Write the queue functions below.


function print() {
    //Print queue elements.
    return collection
}

function enqueue(data) {
    //Print queue elements.
    collection.push(data)
    return collection
}

function dequeue() {
    collection.shift()
    return collection
}

function front() {
    return collection[0]
}

function size() {
    return collection.length
}

function isEmpty() {
    if(collection != -1){
        return false
    }
    else {
        return true
    }
}

module.exports = {
print,
enqueue,
dequeue,
front,
size,
isEmpty
};